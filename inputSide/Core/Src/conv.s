.syntax		unified
.thumb
.section	.text
.align		2
.global conv_as
.type conv_as, %function
/*
void conv_as(const int * x, int nx, const int * h, int nh, int * y);
R0 <-- x
R1 <-- nx
R2 <-- h
R3 <-- nh
R4 <-- y

void conv_as(const int * x, int nx, const int * h, int nh, int * y)
{
	int i, j;
	for (i = 0; i < (nx + nh); i++) {
	y[i] = 0;
	}
	for (i = 0; i < nx; i++) {
		for (j = 0; j < nh; j++) {
			y[i+j] += h[j] * x[i];
		}
	}
}
*/
#define x R0
#define nx R1
#define h R2
#define nh R3
#define y R4
#define i R5
#define j R6

conv_as:
	PUSH {R1-R11, LR}
	/*********************************/
	/* 	for (i = 0; i < (nx + nh); i++) */
	ADD i, nx, nh  /*Generacion del limite del ciclo*/
cleanLoop:
	CBZ i, endCleanLoop
	EOR R7, R7   /*Usamos R7 para colocar ceros en esa posicion*/
	STR R7, [y]  /*Guardamos el cero en la posicion de memoria apuntada por Y */
	ADD y, #4    /*Incrementamos la direccion de memoria apuntada en Y para pasar al siguiente elemento*/
	SUB i, #1    /*Disminuimos en 1 la cantidad de elementos faltantes */
	B cleanLoop  /*Volvemos al principio*/
endCleanLoop:
	/**********************************/
	/*Inicializacion de los indices*/
	EOR i,i
	EOR j,j
	ADD i, i, nx
	ADD j, j, nh
	/*	for (i = 0; i < nx; i++)*/
xSampleLoop:
	CBZ i, endXSampleLoop
	/*	for (j = 0; j < nh; j++) */
	LDR R8, [x]		/*Obtenemos x[i]*/
ySampleLoop:
	CBZ j, endYSampleLoop
	ADD R7, i,j		/*Obtenemos el indice i + j*/
	LDR R9, [h]	    /*Obtenemos h[j]*/
	SMLAL R10, R11, R8, R9  /* y[i+j] += h[j] * x[i];*/
	STR  R10, [y]	/*Nos guardamos los 32 bits mas significativos en y*/
	ADD y, #4 		/*Incrementamos la direccion de memoria apuntada en y para pasar al siguiente elemento*/
	ADD h, #4   	/*Incrementamos la direccion de memoria apuntada en H para pasar al siguiente elemento*/
	SUB j, #1       /*Disminuimos en 1 la cantidad de elementos faltantes */
	B ySampleLoop   /*Volvemos al principio*/
endYSampleLoop:
	ADD x, #4      /*Incrementamos la direccion de memoria apuntada en X para pasar al siguiente elemento*/
	SUB i, #1      /*Disminuimos en 1 la cantidad de elementos faltantes */
	B xSampleLoop  /*Volvemos al principio*/
endXSampleLoop:
	/*Llegados aca, el resultado de la convolucion va a estar guardado en un vector cuya primer posicion es y*/
	/*Entonces termina el programa*/
	POP {R1-R11, PC}
.end




