.syntax		unified
.thumb
.section	.text
.align		2
.global memory_check
.type memory_check, %function

/*
int32_t memory_check(uint32_t * p, uint32_t len)
r0 <- p
r1 <- len
*/
memory_check:
	PUSH {lr}
	MOV R2, #0xAAAAAAAA
	MOV R3, #0X55555555
checkLoop:
	/*Si La cantidad de elementos por chequear es 0 y no fallo antes salimos de la funcion*/
	CBZ R1,endLoopHealthy
	STR R2, [R0]    /*Guardamos el primer valor de prueba en R0 */
	LDR R4, [R0] 	/*Obtenemos el valor del elemento que hay en la posicion a checkear */
	SUB R2,R2,R4    /*Lo comparamos con el valor patron*/
	CBNZ R2,endLoopError  /*Si el valor de la resta da distinto de 0 entonces nos vamos a devolver -1*/
	/*Si llegamos aca entonces el primer checkeo salio bien, procedemos a repetir para el segundo valor*/
	STR R3, [R0]    /*Guardamos el segundo valor de prueba en R0 */
	LDR R4, [R0] 	/*Obtenemos el valor del elemento que hay en la posicion a checkear */
	SUB R3,R3,R4    /*Lo comparamos con el valor patron*/
	CBNZ R3,endLoopError  /*Si el valor de la resta da distinto de 0 entonces nos vamos a devolver -1*/
	SUB R1, #1      /*Disminuimos en una la cantidad de elementos disponibles*/
	B checkLoop     /*Volvemos al principio del lazo*/
endLoopHealthy:
	MOV R0,#0       /*Colocamos un zero en R0 para devolverlo indicando que todo salio bien*/
	POP {PC}
endLoopError:
	MOV R0,#0xFFFFFFFF
	POP {PC}
.end

