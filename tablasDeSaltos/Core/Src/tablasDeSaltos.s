.syntax		unified
.thumb
.section	.text
.align		2
.global getVar
.type getVar, %function

/*int32_t getVar(int32_t lugar)
R0 <-- Lugar
*/

/*
/*int32_t getVar(int32_t lugar)
{
	  switch (lugar) {
		case 0:
			R1 = 10;
			break;
		case 1:
			R1 = 20;
			break;
		case 2:
			R1 = 30;
			break;
		case 3:
			R1 = 40;
			break;
		}
	return R1;
}
*/

#define LUGAR R0
getVar:
	PUSH {LR}
	tbb [PC,LUGAR]
tabla:.byte (dst0-tabla)/2
	  .byte (dst1-tabla)/2
	  .byte (dst2-tabla)/2
	  .byte (dst3-tabla)/2
dst0: LDR R1,=10 /*LUGAR = 0 entonces viene aca*/
	  B fin
dst1: LDR R1,=20 /*LUGAR = 1 entonces viene aca*/
	  b fin
dst2: LDR R1,=30 /*LUGAR = 2 entonces viene aca*/
	  b fin
dst3: LDR R1,=40 /*LUGAR = 3 entonces viene aca*/
	  b fin
fin:
	MOV R0,R1 /*Cargamos el valor de retorno en R0 para devolverlo*/
	POP {PC}
.end
