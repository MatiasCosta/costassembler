.syntax		unified
.thumb
.section	.text
.align		2
.global butterfly
.type butterfly, %function

/*
void butterfly(int32_t * p, int32_t * out);
r0 <-- p
r1 <-- out
*/
/*
    // given an array arr of length n, this code sorts it in place
    // all indices run from 0 to n-1
    for (k = 2; k <= n; k *= 2) // k is doubled every iteration
        for (j = k/2; j > 0; j /= 2) // j is halved at every iteration, with truncation of fractional parts
            for (i = 0; i < n; i++)
                l = bitwiseXOR (i, j); // in C-like languages this is "i ^ j"
                if (l > i)
                    if (  (bitwiseAND (i, k) == 0) AND (arr[i] > arr[l])
                       OR (bitwiseAND (i, k) != 0) AND (arr[i] < arr[l]) )
                          swap the elements arr[i] and arr[l]
*/

#define K R3
#define N R4
#define J R6
#define I R7
#define L R8
#define AUXAND R9
#define AUXCMP R10
butterfly:
	MOV K, #2 /*Almacenamos el valor inicial de K = 2*/
	MOV N, #256 /*Almacenamos el valor maximo de iteraciones*/
	MOV I, #256
kLoop:
	CBZ R5, endKloop
	/********************************************/
jLoop:
	CBNZ J, endJloop
iLoop:
	CBZ I, endILoop
	SUB I, I, #1

	EOR L, I, J

	CMP L,I
	IT LO
	BLO iLoop

	AND AUXAND, I,K

	CMP [I],[L]
	ITE LO
	BLO CMPIFLO
	BLHI CMPIFHI

CMPIFLO:

endCMPIFLO:

CMPIFHI:

endCMPIFHI:

endILoop:
	LSR J, J, #2 /*Dividimos entre 2 a J*/
endJloop:
	/********************************************/
	LSL K, K, #2 /*Se multiplica por 2 el valor de R3*/
	SUB R5, N, K /*Almacenamos en R5, N-K para ver si no se llego a las iteraciones maximas*/
endKloop:

.end




