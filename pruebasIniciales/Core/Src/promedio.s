.syntax		unified
.thumb

.section	.text
.align		2

.global promedio
.type promedio, %function
/*
uint32_t promedio(uint32_t * vector, uint32_t len);
*/
promedio:
	PUSH {LR}
	/*Tengo en R0 la direccion del vector al que le quiero hacer el promedio*/
	/*Tengo en R1 la cantidad de elementos del vector */
	PUSH {R1}
	MOV R2, #0
loopCBZ:
	/*nos fijamos si la cantidad de elementos restantes no es 0*/
	CBZ R1, endCBZ
	/* Cantidad de elementos distinta de cero entonces llegamos aca */
	/* Comenzamos a hacer la suma para el promedio */
	LDR R3,[R0] /*Almacenamos el valor que queremos sumar */
	ADD R0,#4 /*Incrementamos la direccion de memoria para pasar al siguiente elemento*/
	ADD R2, R2, R3 /*Lo sumamos en el registro de sumas */
	SUB R1, #1  /*Disminuimos en una la cantidad de elementos disponibles*/
	B loopCBZ
endCBZ:
	/*Si llegamos aca entonces en R2 esta la suma de todos los valores*/
	/*Entonces volvemos a obtener de la pila la cantidad de elementos originales*/
	/*y efectuamos la division que obtiene el promedio */
	POP {R1}
	SDIV R0, R2, R1 /*Dividimos R2 entre R1, osea la suma sobre la cantidad y lo guardamos en R0*/
	POP {PC}
.end



