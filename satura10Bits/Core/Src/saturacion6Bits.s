.syntax		unified
.thumb
.section	.text
.align		2
.global saturacion6Bits
.type saturacion6Bits, %function
/*
void usat10(int8_t * p, uint32_t len)
r0 <-- p
r1 <-- len
*/


saturacion6Bits:
	PUSH {LR}
	loopCBZ:

	CBZ R1, endLoopCBZ   /*nos fijamos si la cantidad de elementos restantes no es 0*/
	LDRSB R4,[R0] 	     /*Almacenamos el valor a saturar */
	USAT R4,6,R4
	STRB R4,[R0],1       /*guardamos en la direccion de R0, la componente saturada*/
						 /*Tambien incrementamos R0*/

	SUB R1, #1           /*Disminuimos en uno la cantidad de elementos disponibles*/
	B loopCBZ     	     /*Volvemos al principio del lazo*/

	endLoopCBZ:
	POP {PC}
.end
