.syntax		unified
.thumb
.section	.text
.align		2
.global saturacion
.type saturacion, %function
/*
void usat10(int32_t * p, uint32_t len)
r0 <-- p
r1 <-- len
*/

saturacion:
	PUSH {LR}
	loopCBZ:

	CBZ R1, endLoopCBZ /*nos fijamos si la cantidad de elementos restantes no es 0*/
	LDR R4,[R0] 	   /*Almacenamos el valor a saturar */
	USAT R4,10,R4
	STR R4,[R0]        /*guardamos en la direccion de R0, la componente saturada*/

	SUB R1, #1         /*Disminuimos en uno la cantidad de elementos disponibles*/
	ADD R0, #4 		   /*Incrementamos la direccion de memoria para pasar al siguiente elemento*/
	B loopCBZ     	   /*Volvemos al principio del lazo*/

	endLoopCBZ:
	POP {PC}
.end

