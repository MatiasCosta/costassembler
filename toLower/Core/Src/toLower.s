.syntax		unified
.thumb
.section	.text
.align		2
.global toLower
.type toLower, %function

/*toLower("hola como te va");
r0 <--- posicion inicial del string
*/

/**Rango de letras del ascii en minuscula [97-122]*/
/**Rango de letras del ascii en mayuscula = [64-90]*/
#define MAX_ASCII R2
#define MIN_ASCII R3
#define TO_LOWER_N 32

toLower:
	PUSH {LR}
	/*Inicializacion de los indices*/
	MOV MAX_ASCII, 90
	MOV MIN_ASCII, 64
checkLoop:
	LDRB R1, [R0] /*Leemos el primer caracter, ahora esta almacenado en R1*/

	/*Primero verificamos que no sea el caracter nulo, de ser asi entonces debemos volver*/
	CBZ R1, endCheckLoop /*Si es el nulo entonces el byte son todos ceros*/

	/*Ahora debemos verificar que se encuentre en el rango que necesitamos*/
	CMP R1,MAX_ASCII  /*Que no se pase del valor maximo*/
	ITT HI  /*de ser mayor entonces pasamos al siguiente elemento*/
	ADDHI R0,#1 /*Pasamos al siguiente elemento*/
	BHI checkLoop /*Volvemos al principio*/

	CMP R1, MIN_ASCII /*verificamos que no se pase del valor minimo */
	ITT LT /*De ser menor al valor minimo hacemos lo mismo que en el caso de mayor que el maximo*/
	ADDLT R0,#1 /*Pasamos al siguiente elemento*/
	BLT checkLoop /*Volvemos al principio*/

	/*Si llegamos aca entonces tenemos definitivamente una letra, por lo tanto debemos sumarle 32*/
	/*Posteriormente vamos a aumentar el indice y volver al principio*/
	ADD R1, R1, TO_LOWER_N /*Le sumamos 32*/
	STRB R1, [R0]
	ADD R0, #1
	B checkLoop

endCheckLoop:
	POP {PC}
.end

